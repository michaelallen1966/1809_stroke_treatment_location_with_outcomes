import pandas as pd
import numpy as np
from clinical_outcome import Clinical_outcome

outcome = Clinical_outcome()

mimic = np.array([0])
ich = np.array([0.1664])
nlvo = np.array([0.5165])
lvo = np.array([0.3171])
onset_to_needle = np.array([60+71+18]) #71
onset_to_puncture =np.array([90+197])  #195
nlvo_eligible_for_treatment = np.array([0.1936]) * 20.9/20
lvo_eligible_for_treatment = np.array([0.3125]) * 20.9/20
prop_thrombolysed_lvo_receiving_thrombectomy = np.array([1])

outcomes=outcome.calculate_outcome_for_all(
    mimic,
    ich,
    nlvo,
    lvo,
    onset_to_needle,
    onset_to_puncture,
    nlvo_eligible_for_treatment,
    lvo_eligible_for_treatment,
    prop_thrombolysed_lvo_receiving_thrombectomy)

print (outcomes[0])
